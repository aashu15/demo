# Project demo

## Purpose
1. Prepare docker image from offical image from docker registery 
2. Install helloworld java application in container
3. Run it on AWS

## Methodology

### Infrastructure creation
1. Terraform is used to create
	a. Basic networking - VPC
	b. ECS Farget - To run docker container behind an ALB
2. Orchestration
	a. Bash shell scripting
3. Docker image build
	a. Dockerfile
	
### Prerequisites 
1. Linux workstation
2. Properly configured docker daemon and docker daemon is reachable through local docker cli
3. docker command should be in path.
> \>which docker
4. aws cli installed https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
5. at least one aws profile configured
> \> aws configure --profile <profilename>

### How to use it
1. Clone this repository 
>	\> git clone git@bitbucket.org:aashu15/demo.git
2. Go inside demo directory 
>   \> cd demo && bash bin/build_push.sh default latest
>		Where, default -> Name of your aws profile
>			   latest -> docker tag that you want to push
3. Deploy a new tag
>	\> cd demo && bash bin/build_push.sh default 1.0.0
4. Successfull run of above command provide public dns name of ALB, which can be used to access the application

### Design & Customization
1. This module can create
	1. VPC
	2. ECS Cluster
	3. ECR
	4. Deployment on ECS cluster using ECS Fargate - containers in 3 different availability zones
	5. ALB deployed in public subnet to take requests to containers
2. Default Region us-west-2
3. Parameters can be changed in terraform/variable.tf
	1. Region  & AZ
	`
	variable "region" {
  		default = "us-west-2"
	}
	`
	2. VPC CIDR
	`
	variable "cidr" {
  	default     = "10.1.0.0/16"
	}
	`
	3. public subnet
	`variable "public_subnets" {
  		default     = ["10.1.1.0/24","10.1.3.0/24","10.1.5.0/24"]
	}
	4. private subnet
	`
	variable "private_subnets" {
  	default     = ["10.1.2.0/24","10.1.4.0/24","10.1.6.0/24"]
	}
	`
	
	5. availability zones
	`
	variable "azs" {
  		default     = ["us-west-2a","us-west-2b","us-west-2c"]
	}
	`
`


### Possible Enhancements 
1. Terraform modules can be written in more modular format
2. There are a few hard coded values in ECS modules, which can be parameterized
3. If there is no restriction to use Docker registery then **codebuild** and **codedeploy**  can be used to orchestrate end to end CI/CD and deployment.  Totally in a serverless way.

### Reference
1. Terraform registry is used to refer tf code for VPC

