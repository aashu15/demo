#!/bin/bash

export PATH=$PATH:`pwd`/bin/

function docker_build_push {

    echo "************************* Loging to AWS repository *************************"
    $(aws ecr get-login --no-include-email  --profile ${1})

    echo "************************* Building Docker image locally *************************"
    docker build .  -t helloworldjava -f docker/helloworld.dockerfile

    echo "************************* Pushing image to ECR *********************"
    repo_url=`(cd terraform && terraform output demo-repo)`

    if [ $? -eq 0 ]
    then
        docker tag helloworldjava ${repo_url}:${2}

        docker push ${repo_url}:${2}
    else
        echo "Unable to get repo url. Exiting....."
        exit 3
    fi
}


function create_ecr {
    echo "************************* Creating ECR *************************"
    (
        cd terraform && \
        terraform init -input=false && \
        terraform plan -var "aws_profile=${1}" \
                -out terraform.out \
                -target aws_ecr_repository.image_repository \
                -input=false && \
        terraform apply -auto-approve  -input=false terraform.out
    )
}



function create_infra_deploy {
    echo "************************* Building Environment for deployment *************************"

    ####  Build VPC & ECS  - Fargate Infra

    (
        cd terraform && \
        terraform init -input=false && \
        terraform plan -var "aws_profile=${1}" -var "image_tag=${2}"\
            -out terraform.out -input=false && \
        terraform apply -auto-approve  -input=false terraform.out
    )

    echo "************************* Public endpoint for ALB *************************"
    dns_name=`(cd terraform && terraform state show aws_alb.main | grep dns_name | awk -F '=' '{print $NF}')`

    echo $dns_name
}

if [ $# -lt 1 ]
then
    echo "Usage  :: bash build_push.sh <aws profile name> <new-image-tag>"
    echo "           bash build_push.sh aws-test-env 1.0.0"
    exit 1
else
    create_ecr $1
    docker_build_push $1 $2
    create_infra_deploy $1 $2
fi



