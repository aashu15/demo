FROM centos

WORKDIR /opt/app

COPY ./bin/jdk-8u202-linux-x64.rpm ./
RUN yum -y install ./jdk-8u202-linux-x64.rpm 

COPY ./bin/helloworld.war ./

EXPOSE 8080

# Start the app
CMD [ "java", "-jar", "./helloworld.war"]
